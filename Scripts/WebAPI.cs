﻿#if UNITY_ANDROID && !UNITY_EDITOR
#define UNITY_ANDROID_RUNTIME
#endif

using System.Collections;
using System.Text;
using UnityEngine.Networking;

namespace Alien
{
    public static class WebAPI
    {
        public interface IProvider
        {
            string Uri { get; }
            int Timout { get; }
            void Requesting(int count);
            string Encrypt(string password);
            string GetMessage(Error error);
            void OnError(Error error);
        }

        public static void SetProvider<T>() where T : IProvider, new()
            => provider = new T();

        public static void SetProvider(IProvider provider)
            => WebAPI.provider = provider ?? new DefaultProvider();

        public static string Encrypt(string password)
            => provider.Encrypt(password);

        public class DefaultProvider : IProvider
        {
            public virtual string Uri => "http://127.0.0.1:20000";
            public virtual int Timout => 15;
            public virtual void Requesting(int count) => UnityEngine.Debug.Log($"Requesting APIs\n{count}");
            public virtual string GetMessage(Error error) => string.Empty;
            public virtual string Encrypt(string password) => Encrypt(password, "AlienKingdom5076", 9081, 32);
            public virtual string Encrypt(string password, string salt, int iterations, int length)
            {
                using (var pbkdf2 = new System.Security.Cryptography.Rfc2898DeriveBytes(password, Encoding.UTF8.GetBytes(salt), iterations))
                    return System.Convert.ToBase64String(pbkdf2.GetBytes(length));
            }

            public virtual void OnError(Error error)
            {
            }
        }

        public static Response GetResponse(this IEnumerator op)
        {
            while (op.Current is IEnumerator)
                op = op.Current as IEnumerator;
            return op.Current as Response;
        }

        public static Response<T> GetResponse<T>(this IEnumerator op)
        {
            while (op.Current is IEnumerator)
                op = op.Current as IEnumerator;
            if (op.Current is Response<T>)
                return op.Current as Response<T>;
            else if (op.Current is Response)
                return new Response<T>(op.Current as Response);
            else
                return null;
        }

        public class Response
        {
            public readonly bool success;
            public readonly string text;
            public readonly Error error;

            public static implicit operator bool(Response response)
                => response != null && response.success;
            public override string ToString() => text;

            internal Response(Response response)
            {
                success = response.success;
                text = response.text;
                error = response.error;
            }

            internal Response(UnityWebRequest www)
            {
                success = !www.isHttpError && !www.isNetworkError;
                error = www.isNetworkError || www.isHttpError ? GetError(www) : null;
                text = www.downloadHandler.text;
            }

            private static Error GetError(UnityWebRequest www)
            {
                if (www.isNetworkError)
                {
                    return new Error()
                    {
                        isNetworkError = true,
                        message = "network error",
                    };
                }
                var error = new Error() { status = (System.Net.HttpStatusCode)www.responseCode };
                try
                {
                    UnityEngine.JsonUtility.FromJsonOverwrite(www.downloadHandler.text, error);
                }
                catch (System.ArgumentException)
                {
                    error.code = -1;
                    error.message = "unknown error";
                }
                return error;
            }
        }

        public class Response<T> : Response
        {
            public readonly T @object;
            internal Response(Response response) : base(response)
            {
                try
                {
                    @object = UnityEngine.JsonUtility.FromJson<T>(response.text);
                }
                catch (System.ArgumentException)
                {
                }
            }
        }

        [System.Serializable]
        public class Error
        {
            public int code = 0;
            public bool isNetworkError = false;
            public System.Net.HttpStatusCode status = 0;
            public override string ToString() => Message;
            [UnityEngine.SerializeField]
            internal string message;
            public static implicit operator bool(Error error)
                => error != null && (error.isNetworkError || error.code != 0 || error.status >= System.Net.HttpStatusCode.BadRequest);

            public string Message
            {
                get
                {
                    var m = provider.GetMessage(this);
                    return string.IsNullOrEmpty(m) ? message : m;
                }
            }
        }

        public static IEnumerator Get(string path)
        {
            provider.Requesting(++requestingCount);
            using (var www = UnityWebRequest.Get($"{provider.Uri}{path}"))
            {
                www.timeout = provider.Timout;
                www.LoadCookie();
                yield return www.SendWebRequest();
                var response = new Response(www);
                yield return response;
                if (response.error)
                {
                    UnityEngine.Debug.LogError($"{www.responseCode}\t{www.url}\n[{response.error.code}] {response.error.Message}\nraw text:\n{response.text}");
                    provider.OnError(response.error);
                }
                else
                {
                    UnityEngine.Debug.Log($"{www.responseCode}\t{www.url}\nraw text:\n{response.text}");
                }
            }
            provider.Requesting(--requestingCount);
        }

        public static IEnumerator Post(string path, string data = "{}")
        {
            provider.Requesting(++requestingCount);
            using (var www = UnityWebRequest.Post($"{provider.Uri}{path}", ""))
            {
                www.timeout = provider.Timout;
                www.LoadCookie();
                www.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(data)) { contentType = "application/json; charset=utf-8" };
                yield return www.SendWebRequest();
                www.SaveCookie();
                var response = new Response(www);
                yield return response;
                if (response.error)
                {
                    UnityEngine.Debug.LogError($"{www.responseCode}\t{www.url}\n{data}\n[{response.error.code}] {response.error.Message}\nraw text:\n{response.text}");
                    provider.OnError(response.error);
                }
                else
                {
                    UnityEngine.Debug.Log($"{www.responseCode}\t{www.url}\nraw text:\n{response.text}");
                }
            }
            provider.Requesting(--requestingCount);
        }

        public static IEnumerator GraphQL(string query, string variables = null)
        {
            query = queryRegex.Replace(crlfRegex.Replace(query, " "), "$2").Trim();
            if (string.IsNullOrEmpty(variables))
                return Post("/graphql", $@"{{""query"":""{query}""}}");
            else
                return Post("/graphql", $@"{{""query"":""{query}"",""variables"":""{variables.Replace("\\", "\\\\").Replace("\"", "\\\"")}""}}");
        }

        [System.Diagnostics.Conditional("UNITY_ANDROID_RUNTIME")]
        internal static void LoadCookie(this UnityWebRequest www)
        {
            using (var jClass = new UnityEngine.AndroidJavaClass("android.webkit.CookieManager"))
            using (var jObject = jClass.CallStatic<UnityEngine.AndroidJavaObject>("getInstance"))
            {
                var value = jObject.Call<string>("getCookie", provider.Uri);
                if (!string.IsNullOrEmpty(value))
                {
                    var g = cookieRegex.Match(value)?.Groups;
                    if (g != null && g.Count > 1)
                    {
                        UnityWebRequest.ClearCookieCache();
                        www.SetRequestHeader("Cookie", g[0].Value);
                    }
                }
            }
        }

        [System.Diagnostics.Conditional("UNITY_ANDROID_RUNTIME")]
        internal static void SaveCookie(this UnityWebRequest www)
        {
            using (var jClass = new UnityEngine.AndroidJavaClass("android.webkit.CookieManager"))
            using (var jObject = jClass.CallStatic<UnityEngine.AndroidJavaObject>("getInstance"))
            {
                var setCookie = www.GetResponseHeader("Set-Cookie");
                if (!string.IsNullOrEmpty(setCookie))
                {
                    var g = cookieRegex.Match(setCookie)?.Groups;
                    if (g != null && g.Count > 1)
                    {
                        jObject.Call("setAcceptCookie", true);
                        jObject.Call("setCookie", provider.Uri, g[0].Value);
                        jObject.Call("flush");
                    }
                }
            }
        }

        [System.Diagnostics.Conditional("UNITY_ANDROID_RUNTIME")]
        private static void RemoveCookies(this UnityWebRequest www)
        {
            using (var jClass = new UnityEngine.AndroidJavaClass("android.webkit.CookieManager"))
            using (var jObject = jClass.CallStatic<UnityEngine.AndroidJavaObject>("getInstance"))
            {
                jObject.Call("removeAllCookies", null);
                jObject.Call("removeSessionCookies", null);
                jObject.Call("flush");
            }
        }

        private static readonly System.Text.RegularExpressions.Regex cookieRegex = new System.Text.RegularExpressions.Regex("sessionid=(\\w+)", System.Text.RegularExpressions.RegexOptions.Compiled);
        private static readonly System.Text.RegularExpressions.Regex crlfRegex = new System.Text.RegularExpressions.Regex("(\\r*\\n\\s*)", System.Text.RegularExpressions.RegexOptions.Compiled);
        private static readonly System.Text.RegularExpressions.Regex queryRegex = new System.Text.RegularExpressions.Regex("(\\s*)([{|}])(\\s*)", System.Text.RegularExpressions.RegexOptions.Compiled);
        internal static IProvider provider = new DefaultProvider();
        internal static int requestingCount = 0;
    }
}