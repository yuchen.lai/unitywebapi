﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

#pragma warning disable 0649

public static class PsnApi
{
    public const string BaseUrls = "https://asm.np.community.playstation.net/asm/v1/apps/me/baseUrls";
    public const string AuthUrl = "https://auth.api.sonyentertainmentnetwork.com/2.0/oauth/authorize?response_type=token&client_id=656ace0b-d627-47e6-915c-13b259cd06b2&scope=kamaji%3Aaccount_link_token_web&redirect_uri=https%3A%2F%2Fmy%2Eplaystation%2Ecom%2Fauth%2Fresponse%2Ehtml";
    public static string auth;
    public static float fetchProgress = 0f;

    public static IEnumerator UserProfile(System.Action<string, string> onFinish)
    {
        if (string.IsNullOrEmpty(auth))
            yield break;
        var url = $"{BaseUrls}/userProfile";
        using (var www = UnityWebRequest.Get(url))
        {
            www.SetRequestHeader("authorization", auth);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                Debug.LogError(www.downloadHandler.text);
                yield break;
            }
            try
            {
                var response = JsonUtility.FromJson<Response>(www.downloadHandler.text);
                url = $"{response.url}/userProfile/v1/users/me/profile2";
            }
            catch (System.ArgumentException e)
            {
                Debug.LogException(e);
                yield break;
            }
        }
        using (var www = UnityWebRequest.Get(url))
        {
            www.SetRequestHeader("authorization", auth);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                Debug.LogError(www.downloadHandler.text);
                yield break;
            }
            try
            {
                var response = JsonUtility.FromJson<Response>(www.downloadHandler.text);
                onFinish?.Invoke(url.Replace("me", response.profile.onlineId), response.profile.onlineId);
            }
            catch (System.ArgumentException e)
            {
                Debug.LogException(e);
                yield break;
            }
        }
    }

    public static IEnumerator FetchAll(string url, System.Action<int, int> onProgress)
    {
        if (1f - fetchProgress > float.Epsilon && fetchProgress > float.Epsilon)
            yield break;

        fetchProgress = 0f;
        if (string.IsNullOrEmpty(auth))
        {
            onProgress?.Invoke(0, 0);
            yield break;
        }
        onProgress?.Invoke(0, 1);
        string onlineId;
        string[] langs;
        string result;
        using (var www = UnityWebRequest.Get($"{url}?fields=onlineId%2CaboutMe%2ClanguagesUsed%2CtrophySummary(level%2CearnedTrophies)"))
        {
            www.SetRequestHeader("authorization", auth);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                Debug.LogError(www.downloadHandler.text);
                yield break;
            }
            try
            {
                var response = JsonUtility.FromJson<Response>(www.downloadHandler.text);
                onlineId = response.profile.onlineId;
                langs = response.profile.languagesUsed;
                result = www.downloadHandler.text;
            }
            catch (System.ArgumentException e)
            {
                Debug.LogException(e);
                yield break;
            }
        }
        yield return Alien.WebAPI.Post($"/update/2/{onlineId}/", result);
        yield return _TrophyTitles(onlineId, langs, onProgress);
    }

    private static IEnumerator _TrophyTitles(string onlineId, string[] langs, System.Action<int, int> onProgress)
    {
        if (string.IsNullOrEmpty(auth))
            yield break;
        var url = $"{BaseUrls}/trophy";
        using (var www = UnityWebRequest.Get(url))
        {
            www.SetRequestHeader("authorization", auth);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                Debug.LogError(www.downloadHandler.text);
                yield break;
            }
            try
            {
                var response = JsonUtility.FromJson<Response>(www.downloadHandler.text);
                url = $"{response.url}/v1/trophyTitles";
            }
            catch (System.ArgumentException e)
            {
                Debug.LogException(e);
                yield break;
            }
        }
        foreach (var lang in langs)
        {
            var query = $"?fields=%40default%2CtrophyTitleSmallIconUrl&platform=PS3%2CPS4%2CPSVITA&limit=12&npLanguage={lang}";
            for (int i = 0, offset = 0, total = 1; offset < total;)
            {
                TrophyTitle[] trophyTitles;
                string result;
                using (var www = UnityWebRequest.Get($"{url}{query}&offset={offset}"))
                {
                    www.SetRequestHeader("authorization", auth);
                    yield return www.SendWebRequest();
                    if (www.isHttpError || www.isNetworkError)
                    {
                        Debug.LogError(www.downloadHandler.text);
                        yield break;
                    }
                    try
                    {
                        var response = JsonUtility.FromJson<Response>(www.downloadHandler.text);
                        offset += response.limit;
                        total = response.totalResults;
                        trophyTitles = response.trophyTitles;
                        result = www.downloadHandler.text;
                    }
                    catch (System.ArgumentException e)
                    {
                        Debug.LogException(e);
                        yield break;
                    }
                }
                var op = Alien.WebAPI.Post($"/update/2/{onlineId}/", result);
                yield return op;
                var res = Alien.WebAPI.GetResponse(op);
                var ids = new System.Collections.Generic.HashSet<string>(res.text.Split(',', '[', ']', '"'));
                foreach (var trophyTitle in trophyTitles)
                {
                    fetchProgress = i / System.Convert.ToSingle(total);
                    onProgress?.Invoke(i, total);
                    var needUpdate = res && ids.Contains(trophyTitle.npCommunicationId);
                    if (trophyTitle.hasTrophyGroups)
                        yield return _TrophyGroups(url, lang, trophyTitle.npCommunicationId, onlineId, needUpdate);
                    else if (needUpdate)
                        yield return _Trophies(url, lang, "default", trophyTitle.npCommunicationId, onlineId);
                    ++i;
                    fetchProgress = i / System.Convert.ToSingle(total);
                    onProgress?.Invoke(i, total);
                }
            }
        }
    }

    private static IEnumerator _TrophyGroups(string url, string lang, string trophyTitleId, string onlineId, bool needUpdate)
    {
        if (string.IsNullOrEmpty(auth))
            yield break;
        TrophyGroup[] trophyGroups;
        string result;
        using (var www = UnityWebRequest.Get($"{url}/{trophyTitleId}/trophyGroups?fields=%40default%2CtrophyGroupSmallIconUrl&fromUser={onlineId}&npLanguage={lang}"))
        {
            www.SetRequestHeader("authorization", auth);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                Debug.LogError(www.downloadHandler.text);
                yield break;
            }
            try
            {
                var response = JsonUtility.FromJson<Response>(www.downloadHandler.text);
                trophyGroups = response.trophyGroups;
                result = www.downloadHandler.text;
            }
            catch (System.ArgumentException e)
            {
                Debug.LogException(e);
                yield break;
            }
        }
        yield return Alien.WebAPI.Post($"/update/2/{onlineId}/{trophyTitleId}/", result);
        if (needUpdate)
        {
            foreach (var trophyGroup in trophyGroups)
            {
                yield return _Trophies(url, lang, trophyGroup.trophyGroupId, trophyTitleId, onlineId);
            }
        }
    }

    private static IEnumerator _Trophies(string url, string lang, string trophyGroupId, string trophyTitleId, string onlineId)
    {
        if (string.IsNullOrEmpty(auth))
            yield break;
        //yield return new WaitForSeconds(30);
        string result;
        using (var www = UnityWebRequest.Get($"{url}/{trophyTitleId}/trophyGroups/{trophyGroupId}/trophies?fields=%40default%2CtrophyEarnedRate%2CtrophySmallIconUrl&visibleType=1&fromUser={onlineId}&npLanguage={lang}"))
        {
            www.SetRequestHeader("authorization", auth);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                Debug.LogError(www.downloadHandler.text);
                yield break;
            }
            result = www.downloadHandler.text;
        }
        yield return Alien.WebAPI.Post($"/update/2/{onlineId}/{trophyTitleId}/", result);
    }

    [System.Serializable]
    private class Response
    {
        public string url;
        public Profile profile;
        public int totalResults;
        public int offset;
        public int limit;
        public TrophyTitle[] trophyTitles;
        public TrophyGroup[] trophyGroups;
        public Trophy[] trophies;
    }

    [System.Serializable]
    private class Profile
    {
        public string onlineId;
        public string aboutMe;
        public string[] languagesUsed;
        public TrophySummary trophySummary;
    }

    [System.Serializable]
    private class FromUser
    {
        public string onlineId;
        public int progress;
        public Trophies earnedTrophies;
        public bool hiddenFlag;
        public System.DateTime lastUpdateDate;
        public System.DateTime earnedDate;
        public bool earned;
    }

    [System.Serializable]
    private class TrophySummary
    {
        public int level;
        public Trophies earnedTrophies;
    }

    [System.Serializable]
    private class Trophies
    {
        public int bronze;
        public int gold;
        public int platinum;
        public int silver;
    }

    [System.Serializable]
    private class TrophyTitle
    {
        public string npCommunicationId;
        public string trophyTitleName;
        public string trophyTitleDetail;
        public string trophyTitleIconUrl;
        public string trophyTitleSmallIconUrl;
        public string trophyTitlePlatfrom;
        public bool hasTrophyGroups;
        public Trophies definedTrophies;
        public FromUser fromUser;
    }

    [System.Serializable]
    private class TrophyGroup
    {
        public string trophyGroupId;
        public string trophyGroupName;
        public string trophyGroupSmallIconUrl;
        public string trophyGroupDetail;
        public Trophies definedTrophies;
        public FromUser fromUser;
    }

    [System.Serializable]
    private class Trophy
    {
        public string trophyId;
        public string trophyName;
        public string trophyType;
        public string trophySmallIconUrl;
        public string trophyDetail;
        public float trophyEarnedRate;
        public FromUser fromUser;
    }
}